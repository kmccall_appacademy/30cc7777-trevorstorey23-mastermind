class Code
  attr_reader :pegs

  PEGS = {
    red: "R",
    green: "G",
    blue: "B",
    yellow: "Y",
    orange: "O",
    purple: "P"
  }

  def initialize(pegs)
    @pegs = pegs
  end

  def self.parse(color_code_string)
    color_code = color_code_string.upcase.split("")
    if color_code.any? { |color| !PEGS.values.include?(color) }
      raise "Invalid Input"
    else
      Code.new(color_code)
    end
  end

  def self.random
    color_code = []
    until color_code.length == 4
      color_code << PEGS.values[rand(0...PEGS.values.length)]
    end
    Code.new(color_code)
  end

  def [](index)
    @pegs[index]
  end

  def exact_matches(other_code)
    matches = 0
    @pegs.each_with_index do |color, idx|
      matches += 1 if color == other_code[idx]
    end
    matches
  end

  def near_matches(other_code)
    exacts = @pegs.map.with_index do |color, idx|
      color if color == other_code[idx]
    end
    nears = []

    other_code.pegs.each_with_index do |color, idx|

      @pegs.each_with_index do |color2, idx2|
        next if idx2 == idx
        if color == color2 && !nears.include?(color)
          nears << color if !exacts.include?(color)
        end
      end

    end

    nears.length
  end

  def ==(other_code)
    return false if !other_code.instance_of? Code
    return false if @pegs != other_code.pegs
    true
  end
end

class Game
  attr_reader :secret_code

  def initialize(secret_code=Code.random)
    @secret_code = secret_code
  end

  def get_guess
    puts "What's your guess?"
    puts "Your options are R, G, B, Y, O or P"
    guess = gets.chomp
    @user_code = Code.parse(guess)
  end

  def display_matches(code)
    puts "#{code.exact_matches(@secret_code)} exact matches"
    puts "#{code.near_matches(@secret_code)} near matches"
  end

  def play
    turn_count = 10
    until turn_count <= 0 || @user_code.==(@secret_code)
      get_guess
      display_matches(@user_code)
      turn_count -= 1
      puts "#{turn_count} guesses left"
    end
    if @user_code.==(@secret_code)
      puts "You guessed it! #{@secret_code}"
    else
      puts "Sorry, you're out of guesses. The code was #{@secret_code.pegs}"
    end
  end

end
